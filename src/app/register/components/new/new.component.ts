// tslint:disable-next-line: max-line-length
import { Component, OnInit, OnDestroy, Input, ViewChild, AfterViewInit, EventEmitter, OnChanges, SimpleChanges, SimpleChange, ViewEncapsulation } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {
  LoadingService,
  ErrorService,
  ModalService,
  ConfigurationService, ToastService, UserService, AppEventService
} from '@universis/common';
import { Subscription, combineLatest } from 'rxjs';
import { TabsetComponent, TabDirective } from 'ngx-bootstrap/tabs';
import { AdvancedFormComponent } from '@universis/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';
import { cloneDeep } from 'lodash';
import { RegisterService } from '../../services/register.service';
import * as moment from 'moment';
import { EmptyValuePostProcessor } from '@universis/forms';


export interface StudyProgram {
  id: number;
  name: string;
  printableName: string;
  info?: {
    notes?: string,
    locale?: any;
  };
  specialties?: any[];
  department?: {
    currentYear: number;
    currentPeriod: number;
    departmentConfiguration?: {
      nextYear: number;
      nextPeriod: number;
    };
    locale?: any;
  };
  feeCurrency?: {
    alternateName?: any;
    name?: any;
    description?: any;
  }
  fees:number;
  discountCategories?: any[];
  specialtySelectionSemester?: number;
  duration: number;
  startDate: any;
  endDate: any;
}


export interface StudyProgramSpecialization {
  id: string;
  name: string;
  studyProgram: number | any;
  specialty: number;
}

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  constructor(private _context: AngularDataContext,
              private _loading: LoadingService,
              private _errorService: ErrorService,
              private _activatedRoute: ActivatedRoute,
              private _http: HttpClient,
              private _modal: ModalService,
              private _translateService: TranslateService,
              private _router: Router,
              private _configurationService: ConfigurationService,
              private _toastService: ToastService,
              private _registerService: RegisterService,
              private _userService: UserService,
              private _appEvent: AppEventService) { }

  /**
   * Returns the current model state (for insert or update)
   */
  get modelState(): string {
    if (this.model && typeof this.model.id === 'number') {
      return 'update';
    }
    return 'insert';
  }

  get documentState(): string {
    if (this.documents == null) {
      return 'invalid';
    }
    const required = this.documents.filter((item) => {
      return item.numberOfAttachments > 0 && item.attachment == null;
    }).length;
    if (required > 0) {
      return 'invalid';
    }
    return 'valid';
  }

  get activeTab(): any {
    return this.tabset.tabs.find((tab) => {
      return tab.active;
    });
  }
  queryParamSubscription: Subscription;
  formChangeSubscription: Subscription;
  public isLoading = true;
  public selfRegistration = false;
  public createPersonalInfoForm = false;
  public showPeriodValidation = false;
  public showNewMessage = false;
  public showMessagesTab = false;
  public newMessage: { subject?: string; body?: string; attachments: any[]; } = {
    attachments: []
  };
  public messages: any[];
  public studyPrograms: StudyProgram[];
  public selectedStudyProgram: StudyProgram;
  public selectedSpecialization: StudyProgramSpecialization;
  @Input() studyProgramEnrollmentEvent: any;
  public nextButtonDisabled = true;
  public selectProgramDisabled = false;
  public documentStateChange: EventEmitter<any> = new EventEmitter<any>();
  public documents: { attachment: any; attachmentType: any; numberOfAttachments: number }[];
  public specializationSelectionValid = true;
  public formAction: string;

  @Input() model: any = {
    candidate: {
      givenName: null,
      familyName: null,
      email: null
    },
    attachments: [],
    actionStatus: {
      alternateName: 'PotentialActionStatus'
    }
  };
  @ViewChild('tabset', {
    static: true
  }) tabset: TabsetComponent;
  @ViewChild('form1', {
    static: false
  }) form1: AdvancedFormComponent;
  @ViewChild('attachDocuments', {
    static: false
  }) attachDocuments: TabDirective;
  ngOnChanges(changes: SimpleChanges): void {
    //
  }
  ngOnDestroy(): void {
    if (this.queryParamSubscription) {
      this.queryParamSubscription.unsubscribe();
    }
    if (this.formChangeSubscription) {
      this.formChangeSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {

    this.queryParamSubscription = combineLatest(
      this._activatedRoute.queryParams,
      this._activatedRoute.params
    ).subscribe((results) => {
      const queryParams = results[0];
      const params = results[1];
      if (queryParams.show === 'messages') {
        this.showMessagesTab = true;
      }
      // validate code
      if (queryParams.code == null) {
        // loading
        this._loading.showLoading();
        // ping available study programs
        this._registerService.getAvailableStudyPrograms()
          .then((studyPrograms: StudyProgram[]) => {
            // set study programs
            this.studyPrograms = studyPrograms;
            // set form action
            this.formAction = params.action ? `StudyProgramRegisterActions/${params.action}` : `StudyProgramRegisterActions/view`;
            // set self registration
            this.selfRegistration = true;
            this.nextButtonDisabled = false;
            // and hide loading
            this._loading.hideLoading();
            this.isLoading = false;
          }).catch(err => {
            console.error(err);
            // hide loading and navigate to error
            this._loading.hideLoading();
            this.isLoading = false;
            this._errorService.navigateToError(err);
          });
      } else {
        this._loading.showLoading();
        Promise.all([
          this._context.model('StudyProgramRegisterActions').where('code').equal(queryParams.code)
            .expand('specialization', 'employmentStatus',  'ageGroup', 'educationLevel', 'activeStudyLevel',
              'studyProgram($expand=feeCurrency,department($expand=locale), discountCategories($expand=locale,label),info($expand=locale($select=name)))',
              'attachments($expand=attachmentType($expand=locale))',
              'candidate($expand=person,inscriptionMode)',
              'studyProgramEnrollmentEvent($expand=inscriptionYear,articles),discount($expand=locale,label), educationLevel($expand=locale), activeStudyLevel($expand=locale), employmentStatus($expand=locale),attachments($expand=attachmentType($expand=locale))'
              )
            .getItem()
        ]).then((results1) => {
          const item = results1[0];
          this.studyPrograms = [
            item.studyProgram
          ];
          if (item == null) {
            // tslint:disable-next-line:max-line-length
            return this._errorService.navigateToError(new ResponseError('The specified application cannot be found or is inaccessible', 404.5));
          }
          this.selectedSpecialization = item.specialization;
          item.studyProgramEnrollmentEvent.articles = (item.studyProgramEnrollmentEvent.articles || []).filter(x => {
            return x.actionStatus === item.actionStatus.id && x.inLanguage === this._configurationService.currentLocale;
          });
          this.model = item;
          // set form
          this.formAction = params.action ? `StudyProgramRegisterActions/${params.action}` : `StudyProgramRegisterActions/view`;
          this.selectStudyProgram(item.studyProgram, item.studyProgramEnrollmentEvent.id, item.specialization).then(() => {});
          this.nextButtonDisabled = false;
          this.selectProgramDisabled = true;
          // fetch messages
          this.fetchMessages();
          this._loading.hideLoading();
          this.isLoading = false;
        }).catch((err) => {
          this.isLoading = false;
          this._loading.hideLoading();
          return this._errorService.navigateToError(err);
        });
      }
    });
  }

  fetchMessages(): void {
    if (this.model && this.model.id) {
      this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`).asQueryable()
        .orderBy('dateCreated desc').expand('attachments').getItems().then((messages) => {
          this.messages = messages;
        }).catch((err) => {
          console.error(err);
          this.messages = [];
        });
    } else {
      this.messages = [];
    }
  }

  ngAfterViewInit(): void {
    //
  }

  onDataChange(event: any): void {
    //
  }

  async beforeNext(nextTab: TabDirective): Promise<boolean> {

    try {
      if (nextTab.id === 'personal-information') {
        if (this.selfRegistration) {
          this._loading.showLoading();
          console.log(this.model)
          if (this.model.candidate.person == null) {
            // first, try to fetch person data of student, if any
            let me = await this._context.model('Students/me')
            .asQueryable()
            .expand('person($expand=gender)')
            .select('person')
            .getItem();
            if (me && me.person) {
              delete me.person.id;
              // change gender to id (CandidatePerson association with gender has id not identifier)
              if (me.person.gender && me.person.gender.id) {
                me.person.gender = me.person.gender.id;
              }
              else {
                delete me.person.gender;
              }

              Object.assign(this.model.candidate, {
                person: me.person
              });
            } else {
              // then, try to fetch person data of previous candidate, if any
              me = await this._context.model('CandidateStudents/me')
                .asQueryable()
                .expand('person')
                .select('person')
                .getItem();
              if (me && me.person) {
                delete me.person.id;
                Object.assign(this.model.candidate, {
                  person: me.person,
                });
              }
            }
          }
          // assign attributes for form
          Object.assign(this.model, {
            studyProgramEnrollmentEvent: this.studyProgramEnrollmentEvent,
            studyProgram: this.selectedStudyProgram
          });
        }
        this.createPersonalInfoForm = true;
        return true;
      }
      // save application before attach documents
      if (nextTab.id === 'attach-documents') {
        // save action
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
          this._loading.showLoading();
          const formio = this.form1.form.formio;
          const data = formio.data;
          // scroll to error if any
          let errors: any[]=[];
          //find all components with errors
          this.form1.form.formio.on('componentError', (error) => {
            // get the current component with error
            let comp = error.component;
            if (comp) {
              // create array of errors
              errors.push(document.getElementById(comp.id));
            }
            if (Array.isArray(errors)) {
              // σcroll to the first error closest to the top of the page
              const el = errors[0];
              document.getElementById(el.component.id).scrollIntoView({ behavior: 'smooth' })
            }
          });

          formio.setPristine(false);
          this.nextButtonDisabled = !formio.checkValidity(data);
          if (this.nextButtonDisabled) {
            this._toastService.show(this._translateService.instant('InvalidForm.Title'), this._translateService.instant('InvalidForm.Message'));
             // window.scroll(0, 0); -> #line commented because will scroll to error element 
            this._loading.hideLoading();
            this.nextButtonDisabled = !this.nextButtonDisabled;
            return false;
          }
          if (this.selfRegistration) {
            // delete form attributes
            delete this.model.studyProgramEnrollmentEvent;
            delete this.model.studyProgram;
            // get self registration inscription mode (default event mode)
            const selfRegistrationMode = this.studyProgramEnrollmentEvent && this.studyProgramEnrollmentEvent.inscriptionMode;
            if (selfRegistrationMode == null) {
              throw new Error(this._translateService.instant('InvalidInscriptionModeConfiguration'));
            }
            const user = await this._userService.getUser();
            // assign extra data to register action
            Object.assign(data, {
              studyProgram: this.studyProgramEnrollmentEvent.studyProgram && this.studyProgramEnrollmentEvent.studyProgram.id,
              specialization: this.selectedSpecialization || null,
              inscriptionYear: this.studyProgramEnrollmentEvent.inscriptionYear && this.studyProgramEnrollmentEvent.inscriptionYear.id,
              inscriptionPeriod: this.studyProgramEnrollmentEvent.inscriptionPeriod
                && this.studyProgramEnrollmentEvent.inscriptionPeriod.id,
              inscriptionMode: selfRegistrationMode.id || selfRegistrationMode,
              agent: null
            });
            // and to candidate
            Object.assign(data.candidate, {
              inscriptionMode: selfRegistrationMode.id || selfRegistrationMode,
              department: this.studyProgramEnrollmentEvent.studyProgram.department,
              studyProgram: this.studyProgramEnrollmentEvent.studyProgram && this.studyProgramEnrollmentEvent.studyProgram.id,
              inscriptionYear: this.studyProgramEnrollmentEvent.inscriptionYear && this.studyProgramEnrollmentEvent.inscriptionYear.id,
              inscriptionPeriod: this.studyProgramEnrollmentEvent.inscriptionPeriod
                && this.studyProgramEnrollmentEvent.inscriptionPeriod.id,
              studyProgramSpecialty: this.selectedSpecialization || null,
              specialtyId: (this.selectedSpecialization && this.selectedSpecialization.specialty) || null,
              user: user && user.id
            });
            // ensure update if object already exists
            if (this.model
                && this.model.id
                && this.model.candidate
                && this.model.candidate.id
                && this.model.candidate.person
                && this.model.candidate.person.id) {
              data.candidate.id = this.model.candidate.id;
              data.id = this.model.id;
              data.candidate.person.id = this.model.candidate.person.id;
            }
          }
          // parse data with empty value processor
          new EmptyValuePostProcessor().parse(this.form1.formConfig, data);
          // // save application
          const result = await this._context.model('StudyProgramRegisterActions').save(data);
          // update current action
          Object.assign(this.model, result);
          // disable study program selection, if any
          this._appEvent.change.next({
            action: 'DisableStudyProgramSelection',
            target: this.selectedStudyProgram && this.selectedStudyProgram.id
          });
          // and continue
          this._loading.hideLoading();
        }
      }
      return true;
    }
    catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
      return false;
    } finally {
      this._loading.hideLoading();
    }
  }

  next(): void {
    // get active tab
    const findIndex = this.tabset.tabs.findIndex((tab) => {
      return tab.active;
    });
    if (findIndex < this.tabset.tabs.length) {
      // set active tab
      const tab = this.tabset.tabs[findIndex + 1];
      this.beforeNext(tab).then((result) => {
        if (result) {
          tab.disabled = false;
          tab.active = true;
        }
      });
    }
  }

  validateEnrollmentPeriod(enrollmentEvent: { validFrom?: Date; validThrough?: Date }): void {
    // get current date (ignore time)
    const currentDate = new Date().setHours(0, 0, 0, 0);
    let valid = false;
    let started = false;
    if (enrollmentEvent.validFrom instanceof Date) {
      // ignore time of start/end dates also
      const validFrom = enrollmentEvent.validFrom.setHours(0, 0, 0, 0);
      if (enrollmentEvent.validThrough instanceof Date) {
        const validThrough = enrollmentEvent.validThrough.setHours(0, 0, 0, 0);
        valid = validFrom <= currentDate && validThrough >= currentDate;
      } else {
        valid = validFrom <= currentDate;
      }
      if (validFrom <= currentDate) {
        started = true;
      }
    } else if (enrollmentEvent.validThrough instanceof Date) {
      const validThrough = enrollmentEvent.validThrough.setHours(0, 0, 0, 0);
      valid = validThrough >= currentDate;
      if (validThrough >= currentDate) {
        started = true;
      }
    }
    Object.assign(enrollmentEvent, {
      valid,
      started
    });
  }

  selectStudyProgram(studyProgram: StudyProgram, enrollmentEventId: number, specialization?: StudyProgramSpecialization): Promise<any> {
    this._loading.showLoading();
    // find study program
    this.selectedStudyProgram = this.studyPrograms.find((item) => {
      return item.id === studyProgram.id;
    });
    const specializationIndex = (specialization && specialization.specialty) || -1;
    // load study program classes
    if (this.selectedStudyProgram != null) {
      this.selectedSpecialization = specialization;
      return Promise.all([
        this._context.model('StudyProgramEnrollmentEvents')
          .where('id').equal(enrollmentEventId)
          .expand('attachmentTypes($expand=attachmentType($expand=locale))')
          .getItem()
      ])
      .then((results) => {
        // set study program enrollment event
        this.studyProgramEnrollmentEvent = results[0];
        this.ngOnChanges({
          studyProgramEnrollmentEvent: new SimpleChange(null, this.studyProgramEnrollmentEvent, true)
        });
        // validate enrollment period
        if (this.studyProgramEnrollmentEvent) {
          this.validateEnrollmentPeriod(this.studyProgramEnrollmentEvent);
          this.createPersonalInfoForm = true;
        }
        this.onSelectAttachDocuments();
        this._loading.hideLoading();
        return Promise.resolve(this.selectStudyProgram);
      }).catch((err) => {
        // todo:show error
        this._loading.hideLoading();
        return Promise.resolve();
      });
    }
    return Promise.resolve();
  }

  afterStudyProgramSelection(studyProgram: StudyProgram, specialization?: StudyProgramSpecialization): Promise<any> {
    this._loading.showLoading();
    // find study program
    this.selectedStudyProgram = studyProgram;
    if (this.selectedStudyProgram != null) {
      this.selectedSpecialization = specialization;
      return this.getActiveStudyProgramEnrollmentEvent(this.selectedStudyProgram.id).then((studyProgramEnrollmentEvent) => {
        this.studyProgramEnrollmentEvent = studyProgramEnrollmentEvent;
        // validate enrollment period
        if (this.studyProgramEnrollmentEvent) {
          this.validateEnrollmentPeriod(this.studyProgramEnrollmentEvent);
        }
        this.onSelectAttachDocuments();
        this.showPeriodValidation = true;
        this._loading.hideLoading();
        return Promise.resolve(this.selectedStudyProgram);
      }).catch((err) => {
        this.showPeriodValidation = false;
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
        return Promise.resolve();
      });
    }
    return Promise.resolve();
  }

  getActiveStudyProgramEnrollmentEvent(studyProgram: number): Promise<any> {
    return this._context.model('StudyProgramEnrollmentEvents')
      .where('studyProgram').equal(studyProgram)
      .and('eventStatus/alternateName').equal('EventOpened')
      .and('isAccessibleForFree').equal(true)
      .expand('studyProgram($expand=feeCurrency,discountCategories($expand=locale,label)),attachmentTypes($expand=attachmentType($expand=locale))')
      .orderByDescending('id')
      .getItem();
  }



  async onStudyProgramSelection(event): Promise<any> {
    this.showPeriodValidation = false;
    if (event && event.studyProgram) {
      const selected = this.selectedStudyProgram && (this.selectedStudyProgram.id || this.selectedStudyProgram);
      const provided = event.studyProgram.id || event.studyProgram;
      try {
        // tslint:disable-next-line: triple-equals
        if (selected != provided) {
          await this.afterStudyProgramSelection(event.studyProgram);
        } else if (this.studyProgramEnrollmentEvent) {
          this.showPeriodValidation = true;
        }
      } catch (err) {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    } else {
      this.selectedStudyProgram = null;
      this.studyProgramEnrollmentEvent = null;
    }
    this.selectedSpecialization = event && event.specialty;
    this.specializationSelectionValid = (this.selectedStudyProgram == null)
      || (this.selectedStudyProgram.specialtySelectionSemester !== 1)
      || (this.selectedStudyProgram.specialtySelectionSemester === 1 && !!this.selectedSpecialization);
  }

  onFileSelect(event, attachmentType): any {
    // get file
    const addedFile = event.addedFiles[0];
    const formData: FormData = new FormData();
    formData.append('file', addedFile, addedFile.name);
    formData.append('attachmentType', attachmentType.id);
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`StudyProgramRegisterActions/${this.model.id}/addAttachment`);
    this._loading.showLoading();
    return this._http.post(postUrl, formData, {
      headers: serviceHeaders
    }).subscribe((result) => {
      // reload attachments
      this._context.model(`StudyProgramRegisterActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          console.error(err);
          // window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onFileRemove(attachment: any): any {
    this._loading.showLoading();
    // get context service headers
    const serviceHeaders = this._context.getService().getHeaders();
    const postUrl = this._context.getService().resolve(`StudyProgramRegisterActions/${this.model.id}/removeAttachment`);
    return this._http.post(postUrl, attachment, {
      headers: serviceHeaders
    }).subscribe(() => {
      // reload attachments
      this._context.model(`StudyProgramRegisterActions/${this.model.id}/attachments`)
        .asQueryable().expand('attachmentType')
        .getItems()
        .then((attachments) => {
          // set model attachments
          this.model.attachments = attachments;
          // refresh attachment
          this.onSelectAttachDocuments();
          // hide loading
          this._loading.hideLoading();
        }).catch((err) => {
          window.location.reload();
          this._loading.hideLoading();
        });
    }, (err) => {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    });
  }

  onSelectAttachDocuments(): void {
    this.documents = this.documents || [];
    // if study program enrollment event is not not defined
    if (this.studyProgramEnrollmentEvent == null) {
      // exit
      return;
    }
    this.documents = this.studyProgramEnrollmentEvent.attachmentTypes.map((item: any) => {
      const res = {
        attachmentType: item.attachmentType,
        numberOfAttachments: item.numberOfAttachments
      };
      // try to search model attachments list
      const findAttachment = this.model.attachments.find((attachment) => {
        if (attachment.attachmentType == null) {
          return false;
        }
        return attachment.attachmentType.id === item.attachmentType.id;
      });
      if (findAttachment) {
        // assign data
        Object.assign(res, {
          attachment: findAttachment
        });
      }
      return res;
    });
    this.documentStateChange.emit(this.documentState);
  }

  submit(): any {
    const SubmitTitle = this._translateService.instant('SubmitTitle');
    const SubmitTitleMessage = this._translateService.instant('SubmitTitleMessage');
    this._modal.showSuccessDialog(SubmitTitle, SubmitTitleMessage).then((result) => {
      if (result === 'ok') {
        this._loading.showLoading();
        const model = cloneDeep(this.model);
        // remove attachments
        delete model.attachments;
        // set action status
        model.actionStatus = {
          alternateName: 'ActiveActionStatus'
        };
        this._context.model('StudyProgramRegisterActions').save(model).then(() => {
          // navigate to list
          this._loading.hideLoading();
          return this._router.navigate([
            '/'
          ]);
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onSelectTab(event: any): void {
    debugger
    switch (event.id) {
      case 'personal-information':
        if (this.model.actionStatus.alternateName === 'PotentialActionStatus') {
          const find = this.tabset?.tabs.find((tab) => {
            return tab.id === 'attach-documents';
          });
          if (find) {
            find.disabled = true;
          }
        }
        break;
    }
  }

  downloadAttachment(attachment: any): void {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status !== 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }


  async send(message: any): Promise<void> {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: {
          name: 'Registrar'
        }
      });
      await this._context.model(`StudyProgramRegisterActions/${this.model.id}/messages`).save(message);
      // reload message
      this.fetchMessages();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

}
