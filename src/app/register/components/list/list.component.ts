import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ActivatedUser, ConfigurationService, LoadingService} from '@universis/common';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  constructor(private _context: AngularDataContext,
              private _activatedUser: ActivatedUser,
              private _loading: LoadingService,
              private _configurationService: ConfigurationService) { }

  public items: any[] = [];
  public loading = true;
  public canSelfRegister = true;

  ngOnInit(): void {
    this._activatedUser.user.subscribe((user) => {
      if (user) {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions')
          .where('owner').equal(user.id)
          .orderBy('dateModified desc')
          .expand('actionStatus', 'specialization', 'inscriptionYear', 'inscriptionPeriod', 'studyProgram($expand=subjectArea($expand=locale), department($expand=locale), studyLevel,teachingMethod($expand=locale($select=name)), info($expand=locale($select=name))),',
            'studyProgramEnrollmentEvent($expand=articles)').getItems()
          .then((items) => {
            items.map(item => {
              // filter item articles by language and actionStatus
              item.studyProgramEnrollmentEvent.articles = (item.studyProgramEnrollmentEvent.articles || []).filter(x => {
                return x.actionStatus === item.actionStatus.id && x.inLanguage === this._configurationService.currentLocale;
              });
              return item;
            });
            this.items = items;
            this.loading = false;
            this._loading.hideLoading();
          });
      }
    });
  }
}
