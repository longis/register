import {
  Component,
  Input,
  OnInit,
  Output,
  EventEmitter,
  ViewChild,
  ViewEncapsulation,
  OnDestroy,
} from '@angular/core';
import { AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { Subscription } from 'rxjs';
import { StudyProgram, StudyProgramSpecialization } from '../new/new.component';

@Component({
  selector: 'app-select-study-program',
  templateUrl: './select-study-program.component.html',
  styleUrls: ['./select-study-program.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class SelectStudyProgramComponent implements OnInit, OnDestroy {
  // get available study programs as input
  @Input('studyPrograms') studyPrograms: StudyProgram[];
  // get selected study program as input (if any)
  @Input('selectedStudyProgram') selectedStudyProgram: StudyProgram;
  // get selected study program specialty as input (if any)
  @Input('selectedSpecialty') selectedSpecialty: {
    id: number | string;
    name: string;
    specialty: number;
  };
  // communicate with an output with child components about study program selection (if any)
  @Output() onStudyProgramSelection: EventEmitter<{
    studyProgram: StudyProgram;
    specialty: StudyProgramSpecialization;
  }> = new EventEmitter<{
    studyProgram: StudyProgram;
    specialty: StudyProgramSpecialization;
  }>();
  // get formComponent as ViewChild
  @ViewChild('formComponent') formComponent: AdvancedFormComponent;

  public selectedStudyProgramData: {
    selectedStudyProgram: StudyProgram;
    selectedSpecialty: {
      id: number | string;
      name: string;
      specialty: number;
    };
  };
  public locallySelectedStudyProgram: StudyProgram;

  private _previousSubjectAreaState: number | string;
  private _disableSelectionSubscription: Subscription;

  constructor(private readonly _appEvent: AppEventService) {}

  ngOnInit(): void {
    // create an object of the selected study program for formio
    this.selectedStudyProgramData = {
      selectedStudyProgram: this.selectedStudyProgram,
      selectedSpecialty: this.selectedSpecialty,
    };
    // update previous state and currently selected study program, if any
    if (this.selectedStudyProgram) {
      this.locallySelectedStudyProgram = this.selectedStudyProgram;
    }
    // open a subscription to disable the study program selection
    this._disableSelectionSubscription = this._appEvent.change.subscribe(
      (change: { action: string; target: number | string }) => {
        if (
          change &&
          change.action === 'DisableStudyProgramSelection' &&
          this.locallySelectedStudyProgram &&
          change.target === this.locallySelectedStudyProgram.id
        ) {
          this.reset();
        }
      }
    );
  }

  onDataChange(event: any): void {
    const data =
      this.formComponent &&
      this.formComponent.form &&
      this.formComponent.form.formio &&
      this.formComponent.form.formio.data;
    if (data == null) {
      return;
    }
    // get study program after change
    let targetStudyProgram: StudyProgram = data.studyProgram;
    const targetProgramSpecialty: StudyProgramSpecialization = data.studyProgramSpecialty;
    // get study level
    const subjectArea: number | string = data.subjectArea;
    if (!subjectArea && !!targetStudyProgram) {
      // handle instant subjectArea deletion
      targetStudyProgram = null;
    }
    if (subjectArea !== this._previousSubjectAreaState && !!targetStudyProgram) {
      // handle instant subjectArea change
      targetStudyProgram = null;
    }
    // set previous state
    this._previousSubjectAreaState = subjectArea;
    // emit change to output
    this.onStudyProgramSelection.emit({
      studyProgram: targetStudyProgram,
      specialty: targetProgramSpecialty
    });
    // and set the locally selected study program
    this.locallySelectedStudyProgram = targetStudyProgram;
  }

  private reset(): void {
    if (this.formComponent) {
      this.formComponent.ngOnDestroy();
      this.selectedStudyProgramData = {
        selectedStudyProgram: this.locallySelectedStudyProgram,
        selectedSpecialty: this.selectedSpecialty,
      };
      this.formComponent.ngOnInit();
    }
  }

  ngOnDestroy(): void {
    if (this._disableSelectionSubscription) {
      this._disableSelectionSubscription.unsubscribe();
    }
  }
}
