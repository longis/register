# universis/applicants

Applicants module of Universis Project.

## Description

This module is responsible for managing and reviewing applications for a programme of study made by the end-users.

# Development

This module is standard Angular module. To run it, use the following command:

```bash
npm run ng serve applicants
```

# Production

Build the module using the following command:

```bash
npm run build:applicants
```

This operation will create a new directory in the `dist/register` folder with the name `applicants`. This directory contains the compiled module which will be available as a part of the main application.