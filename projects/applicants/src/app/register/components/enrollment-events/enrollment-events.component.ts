import { Component, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedUser, ConfigurationService, LoadingService } from '@universis/common';

@Component({
  selector: 'app-register-enrollment-events',
  templateUrl: './enrollment-events.component.html',
  styleUrls: ['./enrollment-events.component.scss']
})
export class EnrollmentEventsComponent implements OnInit {
  constructor(private _context: AngularDataContext,
              private _activatedUser: ActivatedUser,
              private _loading: LoadingService,
              private _configurationService: ConfigurationService) { }

  public items: any[] = [];
  public studyProgramRegisterActions: any[] = [];
  public loading = true;

  ngOnInit(): void {
    this._activatedUser.user.subscribe(async (user) => {
      if (user) {
        this._loading.showLoading();
        try {
          this.items = await this._context
            .model('StudyProgramEnrollmentEvents')
            .asQueryable()
            .expand('viewers,organizer,studyProgram($expand=studyLevel($select=id,name,locale)),inscriptionPeriod($select=id,name,locale)')
            .filter('viewers/id eq me() and eventStatus/alternateName eq \'EventOpened\'')
            .getItems().then((items) => {
              this.loading = false;
              this._loading.hideLoading();
              return items;
            });
          for (const item of this.items) {
            // Get requests
            const registerAction = await this._context
              .model(`StudyProgramEnrollmentEvents/${item.id}/showRequests`)
              .select('count(id) as requestCount,actionStatus/alternateName as actionStatus')
              .groupBy('actionStatus/alternateName')
              .getItems();

            // Count total number of requests
            registerAction.actions = registerAction.reduce((total: number, action) => total + Number(action.requestCount), 0);
            // Make a property that has actionStatus as key and requestCount as value
            for (const action of registerAction) {
              registerAction[action.actionStatus] = action.requestCount;
            }
            item.registerAction = registerAction;
          }
        } catch (err) {
          console.error(err);
        }
      }
    });
  }
}
