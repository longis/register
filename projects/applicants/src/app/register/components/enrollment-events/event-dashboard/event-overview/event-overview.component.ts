import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-register-enrollment-events-event-overview',
  templateUrl: './event-overview.component.html'
})
export class EnrollmentEventOverviewComponent implements OnInit {
  public enrollmentEvent: any;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit(): Promise<void> {
    const enrollmentEvent = await this._context.model('StudyProgramEnrollmentEvents')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('studyProgram,inscriptionModes')
      .getItem();
    this.enrollmentEvent = enrollmentEvent;
  }
}
