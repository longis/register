import { Component, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {UserActivityService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-register-enrollment-events-enrollment-events-root',
  templateUrl: './event-root.component.html'
})
export class EnrollmentEventRootComponent implements OnInit {

  public enrollmentEvent: any;
  public tabs: any[];
  public enrollmentEventId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _userActivityService: UserActivityService,
              private _translateService: TranslateService,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.enrollmentEventId = this._activatedRoute.snapshot.params.id;

    try {
      this.enrollmentEvent = await this._context
        .model('StudyProgramEnrollmentEvents')
        .where('id').equal(this.enrollmentEventId)
        .expand('viewers,organizer,studyProgram($expand=studyLevel($select=id,name,locale)),inscriptionPeriod($select=id,name,locale)')
        .getItem();

      const registerAction = await this._context
        .model(`StudyProgramEnrollmentEvents/${this.enrollmentEvent.id}/showRequests`)
        .select('count(id) as requestCount,actionStatus/alternateName as actionStatus')
        .groupBy('actionStatus/alternateName')
        .getItems();

      // Count total number of requests
      registerAction.actions = registerAction.reduce((total: number, action) => total + Number(action.requestCount), 0);
      // Make a property that has actionStatus as key and requestCount as value
      for (const action of registerAction) {
        registerAction[action.actionStatus] = action.requestCount;
      }
      this.enrollmentEvent.registerAction = registerAction;
    } catch (err) {
      console.error(err);
    }

    this.tabs = this._activatedRoute.routeConfig.children.filter(route => typeof route.redirectTo === 'undefined');

    // save user activity for events
    return this._userActivityService.setItem({
      category: this._translateService.instant('Candidates.TitleSingular'),
      description: this._translateService.instant(this.enrollmentEvent.name),
      url: window.location.hash.substring(1),
      dateCreated: new Date()
    });
  }

}
