import { Component, OnInit, OnDestroy, Input, ViewEncapsulation, ViewChild, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ModalService, DIALOG_BUTTONS, LoadingService, ErrorService, UserService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { DataServiceQueryParams, ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';
import { AppEventService } from '@universis/common';
import { AdvancedFormComponent } from '@universis/forms';
import { Observable } from 'rxjs';
import JSZip = require('jszip');
import { RequestActionComponent } from '../request-action/request-action.component';

const EXPAND_INTERNSHIP_REGISTRATIONS = 'internshipRegistrations($select=id,initiator,actionStatus,internship/name as internshipName,reviews($expand=createdBy),internship/company/name as companyName,internship/company/country/name as countryName;$expand=actionStatus)';

@Component({
  selector: 'app-register-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EditComponent implements OnInit, OnDestroy {
  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _router: Router,
    private _modal: ModalService,
    private _loading: LoadingService,
    private _errorService: ErrorService,
    private _userService: UserService,
    private _translateService: TranslateService,
    private _http: HttpClient,
    private _appEvent: AppEventService
  ) { }
  public static readonly ServiceQueryParams = {
    $expand: 'studyProgram,specialization,inscriptionYear,inscriptionPeriod,messages,reviews($expand=createdBy),candidate($expand=person),attachments($expand=attachmentType)'
  };
  private requestActionId: string;
  private enrollmentEventId: string;

  dataSubscription: any;
  paramSubscription: any;
  editingReview: any = false;
  @ViewChild('form') form: AdvancedFormComponent;
  @Input() model: any;
  @Input() showNavigation = true;
  @Input() showActions = true;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public userId;
  public messages: any[];
  public otherApplications: any[];
  newMessage: any = {
    attachments: []
  };
  public showNewMessage = false;
  public activeCourseRegistration: any;
  public activeInternshipRegistration: any;
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  async ngOnInit(): Promise<void> {
    this.userId = await this._userService.getUser().then((viewer) => viewer.id);
    this.paramSubscription = this._activatedRoute.params.subscribe((query) => {
      if (query && query.target) {
        this.activeCourseRegistration = parseInt(query.target, 10);
      } else {
        this.activeCourseRegistration = null;
      }
      if (query && query.targetInternship) {
        this.activeInternshipRegistration = parseInt(query.targetInternship, 10);
      } else {
        this.activeInternshipRegistration = null;
      }
    });
    this.dataSubscription = this._activatedRoute.data.subscribe(async (data) => {
      this.enrollmentEventId = this._activatedRoute.parent.parent.snapshot.params.id;
      this.requestActionId = this._activatedRoute.snapshot.params.id;
      try {
        data.model = await this._context.model(`StudyProgramEnrollmentEvents/${encodeURIComponent(this.enrollmentEventId)}/showRequests`)
          .where('id')
          .equal(this.requestActionId)
          .expand('attachments($expand=attachmentType($select=id,name))')
          .getItem();
        this.model = data.model;
        if (this.form) {
          this.form.refreshForm.emit({
            submission: {
              data: this.model
            }
          });
        }
        // get messages
        if (this.model) {

          // get other active applications of the same user
          this._context.model(`StudyProgramEnrollmentEvents/${encodeURIComponent(this.enrollmentEventId)}/showRequests`)
            .where('id').equal(this.requestActionId)
            .where('inscriptionYear').equal(this.model.inscriptionYear.id)
            .and('inscriptionPeriod').equal(this.model.inscriptionPeriod.id)
            .and('owner').equal(this.model.owner)
            .and('actionStatus/alternateName').equal('ActiveActionStatus')
            .expand('studyProgram', 'specialization', 'reviews($expand=createdBy)', 'attachments($expand=attachmentType($select=id,name))')
            .orderByDescending('dateModified')
            .getItems().then((results) => {
              this.otherApplications = results.filter((item) => {
                return item.id !== this.model.id;
              });
            });

          this.messages = this.model.messages;
        }
      } catch (err) {
        console.log(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
      }
    });
  }

  myReview(): boolean {
    const findIt = this.model.reviews.find(review => review.createdBy.id === this.userId);
    if (findIt !== undefined) {
      return true;
    }
    return false;
  }

  reload(): Promise<void> {
    if (this.model == null) {
      // do nothing
      return;
    }
    return this._context.model(`StudyProgramEnrollmentEvents/${encodeURIComponent(this.enrollmentEventId)}/showRequests`)
      .where('id').equal(this.model.id)
      .expand('attachments($expand=attachmentType($select=id,name))')
      .getItem().then((result) => {
        this.model = result;
      }).catch((err) => {
        console.error(err);
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

  preview(attachment: any): Promise<boolean> {
    return this._router.navigate(['.'], {
      replaceUrl: false,
      relativeTo: this._activatedRoute,
      skipLocationChange: true,
      queryParams: {
        download: attachment.url
      }
    });
  }

  download(attachment: any): void {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  /**
   * Function that is used when user selects the download all attachment option.
   */
  async downloadAction(): Promise<void> {
    try {
      this._modal.openModalComponent(RequestActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.model.attachments,
          modalTitle: 'Requests.Edit.DownloadAttachmentAction.Title',
          description: 'Requests.Edit.DownloadAttachmentAction.Description',
          refresh: this.refreshAction,
          execute: this.downloadDocuments()
        }
      });
    } catch (err) {
      console.error(err);
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  private downloadDocuments(): Observable<unknown> {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 0
      });

      const total: number = this.model.attachments.length;
      const result: { total: number; success: number; errors: number } = {
        total,
        success: 0,
        errors: 0
      };

      (async () => {
        const zip: JSZip = new JSZip();
        const headers: Headers = this._context.getService().getHeaders();
        const a: HTMLAnchorElement = document.createElement('a');
        const getDocumentBlob = async (documentToDownload): Promise<Blob> => {
          let documentUrl: string = documentToDownload.url.replace('/api', '');
          documentUrl = this._context.getService().resolve(documentUrl);

          const response: Response = await fetch(documentUrl, {
            headers
          });

          if (!response.ok) {
            throw new Error(response.statusText);
          }

          return await response.blob();
        };

        if (this.model.attachments.length > 1) {
          for (let i = 0; i < this.model.attachments.length; i++) {
            try {
              const documentToDownload = this.model.attachments[i];
              const documentBlob: Blob = await getDocumentBlob(documentToDownload);

              // tslint:disable-next-line:max-line-length
              zip.file(`${documentToDownload.attachmentType.name.replace(/[~!@#$%^&*()|+\-=?;:'",.<>{}[\]\/]/gi, '_')}_${documentToDownload.id}.pdf`, documentBlob);
              this.refreshAction.emit({
                progress: Math.floor(((i + 1) / total) * 100)
              });
              result.success += 1;
            } catch (err) {
              console.error(err);
              result.errors += 1;
            }
          }
          const zipFile: Blob = await zip.generateAsync({type: 'blob'});

          a.href = URL.createObjectURL(zipFile);
          a.download = `${this.model.familyName && this.model.givenName ? this.model.familyName + '-' + this.model.givenName : 'document'}.zip`;
        } else if (this.model.attachments.length === 1) {
          const documentBlob: Blob = await getDocumentBlob(this.model.attachments[0]);

          a.href = URL.createObjectURL(documentBlob);
          // tslint:disable-next-line:max-line-length
          a.download = `${this.model.attachments[0].attachmentType.name.replace(/[~!@#$%^&*()|+\-=?;:'",.<>{}[\]\/]/gi, '_')}_${this.model.attachments[0].id}.pdf`;
          result.success += 1;
        }
        a.click();
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  revert(): void {
    const RevertConfirm = this._translateService.instant('Register.RevertConfirm') || {
      Title: 'Activate application',
      Message: 'You are going to activate this application. Do you want to proceed?'
    };
    this._modal.showDialog(RevertConfirm.Title, RevertConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'ActiveActionStatus'
          }
        }).then(async () => {
          // reload page
          try {
            await this.reload();
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          } catch (err) {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              Title: 'Refresh failed',
              Message: 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          }
        }).catch((err): void => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  reset(): void {
    const ResetConfirm = this._translateService.instant('Register.ResetConfirm') || {
      Title: 'Change application status',
      Message: 'You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?'
    };
    this._modal.showDialog(ResetConfirm.Title, ResetConfirm.Message, DIALOG_BUTTONS.YesNo, {
      theme: 'modal-dialog-danger'
    }).then((result) => {
      if (result === 'yes') {
        this._loading.showLoading();
        this._context.model('StudyProgramRegisterActions').save({
          id: this.model.id,
          actionStatus: {
            alternateName: 'PotentialActionStatus'
          }
        }).then(async () => {
          // reload page
          try {
            await this.reload();
            this._loading.hideLoading();
            // send an application event
            this._appEvent.change.next({
              model: 'StudyProgramRegisterActions',
              target: this.model
            });
          } catch (err) {
            this._loading.hideLoading();
            const ReloadError = this._translateService.instant('Register.ReloadError') || {
              Title: 'Refresh failed',
              Message: 'The operation has been completed successfully but something went wrong during refresh.'
            };
            this._modal.showErrorDialog(ReloadError.Title, ReloadError.Message, DIALOG_BUTTONS.Ok).then(() => {
              this._router.navigate(['/candidates']);
            });
          }
        }).catch((err) => {
          this._loading.hideLoading();
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
      }
    });
  }

  onItemReviewEvent(event): void {
    if (event.data && event.data.cancel === true) {
      this.editingReview = false;
    } else if (event.data && event.data.addReview === true) {
      // add or update review
      this._context
        .model(`StudyProgramEnrollmentEvents/${this.enrollmentEventId}/requests/${this.requestActionId}/review`)
        .save(event.data)
        .then(() => {
          this.editingReview = false;
          this.reload();
        }).catch((err) => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
  }

  async sendWithoutAttachment(message): Promise<void> {
    try {
      this._loading.showLoading();
      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: this.model.owner
      });
      await this._context.model(`StudyProgramEnrollmentEvents/${this.enrollmentEventId}/requests/${this.model.id}/message`).save(message);
      // reload message
      this.messages = await this._context.model(`StudyProgramEnrollmentEvents/${this.enrollmentEventId}/requests/${this.model.id}/messages`)
        .asQueryable()
        .getItems();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async send(message): Promise<void> {
    try {
      if (!(message.attachments && message.attachments.length)) {
        // send message without attachment
        await this.sendWithoutAttachment(message);
        return;
      }
      this._loading.showLoading();

      // set recipient (which is this action owner)
      Object.assign(message, {
        recipient: this.model.owner
      });

      const formData: FormData = new FormData();
      // get attachment if any
      if (message.attachments && message.attachments.length) {
        formData.append('attachment', message.attachments[0], message.attachments[0].name);
      }
      Object.keys(message).filter((key) => {
        return key !== 'attachments';
      }).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(message, key)) {
          formData.append(key, message[key]);
        }
      });
      // get context service headers
      const serviceHeaders = this._context.getService().getHeaders();
      const serviceUrl = this._context.getService().resolve(`StudyProgramEnrollmentEvents/${this.enrollmentEventId}/requests/${this.model.id}/sendMessage`);
      await this._http.post(serviceUrl, formData, {
        headers: serviceHeaders
      }).toPromise();
      // reload message
      this.messages = await this._context.model(`StudyProgramEnrollmentEvents/${this.enrollmentEventId}/requests/${this.model.id}/messages`)
        .asQueryable()
        .getItems();
      // clear message
      this.newMessage = {
        attachments: []
      };
      this.showNewMessage = false;
      this._loading.hideLoading();
    } catch (err) {
      this._loading.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }

  }

  downloadFile(attachment): void{
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    this._loading.showLoading();
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {
      if (response.status !== 200) {
        throw new ResponseError(response.statusText, response.status);
      }
      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
        this._loading.hideLoading();
      }).catch((err) => {
        this._loading.hideLoading();
        this._errorService.showError(err, {
          continueLink: '.'
        });
      });
  }

}
