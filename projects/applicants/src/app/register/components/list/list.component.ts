import { Component, AfterViewInit, ViewChild, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import {
  AdvancedFilterValueProvider,
  AdvancedRowActionComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import { AdvancedTableSearchComponent } from '@universis/ngx-tables';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import { Subscription, Observable } from 'rxjs';
import { AngularDataContext } from '@themost/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { AppEventService, LoadingService, ModalService, ErrorService, UserService } from '@universis/common';
import { ActivatedTableService } from '@universis/ngx-tables';
import { ClientDataQueryable } from '@themost/client';
import { SendMessageActionComponent } from '../send-message-action/send-message-action.component';
import * as moment from 'moment';
import * as config from '../../../../assets/tables/StudyProgramRegisterActions/config.list.json';
@Component({
  selector: 'app-register-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements AfterViewInit, OnDestroy {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  private changeSubscription: Subscription;
  @ViewChild('table') table!: AdvancedTableComponent;
  @ViewChild('search') search!: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch!: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  selectedItems: any[];
  public enrollmentEvent: any;
  @Input() enrollmentEventId: any;
  userId: any;

  constructor(
    private _context: AngularDataContext,
    private _router: Router,
    private _activatedTable: ActivatedTableService,
    private _activatedRoute: ActivatedRoute,
    private _appEvent: AppEventService,
    private _advancedFilter: AdvancedFilterValueProvider,
    private _userService: UserService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService
  ) { }

  async ngAfterViewInit(): Promise<void> {
    this.enrollmentEventId = this._activatedRoute.parent.snapshot.params.id;
    this._advancedFilter.values = {
      ...this._advancedFilter.values,
      id: this.enrollmentEventId
    };

    this.userId = await this._userService.getUser().then((viewer) => viewer.id);

    // check if current user has permission to delete register action
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      try {
        this.table.config = AdvancedTableConfiguration.cast(config, true);
        this.table.config.model = `StudyProgramEnrollmentEvents/${this.enrollmentEventId}/showRequests`;
        this.table.config.columns.find(column => column.property === 'myReview')
          .formatters[0].formatString = '${reviews && Array.isArray(reviews) && reviews.length > 0 ? reviews.map(review=> review.createdBy.id).find(id => id === ' + this.userId + ') ? reviews.find(review => review.createdBy.id === ' + this.userId + ').reviewRating : "-" : "-"}';
        this._activatedTable.activeTable = this.table;
        this.table.ngOnInit();
      } catch (err) {
        console.error(err);
      }
    });

    this.changeSubscription = this._appEvent.changed.subscribe((event) => {
      if (this.table.dataTable == null) {
        return;
      }
      if (event && event.target && event.model === 'StudyProgramRegisterActions') {
        this.table.fetchOne({
          result: event.target.id
        });
      }
      if (event && event.target && event.model === this.table.config.model) {
        this.table.fetchOne({
          id: event.target.id
        });
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult): void {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'actionStatus/alternateName as actionStatus',
            'owner', 'candidate/id as candidateIdentifier', 'candidate/person/email as email',
            'studyProgramEnrollmentEvent/inscriptionYear/id as inscriptionYearId',
            'candidate/department/currentYear/id as currentYear', 'reviews'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              actionStatus: item.actionStatus,
              owner: item.owner,
              candidateIdentifier: item.candidateIdentifier,
              email: item.email,
              inscriptionYearId: item.inscriptionYearId,
              currentYear: item.currentYear,
              reviews: item.reviews
            };
          });
        }
      }
    }
    return items;
  }

  executeChangeActionStatus(statusText: string) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            // set active item
            const updated = {
              id: item.id,
              actionStatus: {
                alternateName: statusText
              }
            };
            await this._context.model(this.table.config.model).save(updated);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: updated.id
              });
            } catch (err) {
              //
            }

          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  executeSendMessageAction(message) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const newMessage = Object.assign({}, message, {
              recipient: item.owner
            });
            await await this._context.model(`StudyProgramRegisterActions/${item.id}/messages`).save(newMessage);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  validateEnrollmentPeriod(enrollmentPeriod): boolean {
    const now = moment(new Date()).startOf('day').toDate();
    let valid = false;
    if (enrollmentPeriod.validFrom instanceof Date) {
      if (enrollmentPeriod.validThrough instanceof Date) {
        valid = enrollmentPeriod.validFrom <= now && enrollmentPeriod.validThrough >= now;
      } else {
        valid = enrollmentPeriod.validFrom <= now;
      }
    } else if (enrollmentPeriod.validThrough instanceof Date) {
      valid = enrollmentPeriod.validThrough >= now;
    }
    return valid;
  }

  async sendMessageAction(): Promise<void> {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.ComposeNewMessage.Title',
          description: 'Register.ComposeNewMessage.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendMessageAction(message)
        }
      });
    } catch (err) {
      console.log(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeSendDirectMessageAction(message) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const newMessage = Object.assign({}, message, {
              recipient: item.owner,
              candidateStudent: item.candidateIdentifier
            });
            await this._context.model(`CandidateStudents/${item.candidateIdentifier}/sendMessage`).save(newMessage);
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }

        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async sendDirectMessageAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // regex source: https://github.com/formio/formio.js/blob/master/src/validator/rules/Email.js#L13
      // tslint:disable-next-line: max-line-length
      const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      this.selectedItems = items.filter(candidate => re.test(candidate.email));
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.SendDirectMessageToCandidate.Title',
          description: 'Register.SendDirectMessageToCandidate.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendDirectMessageAction(message)
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async deleteRegisterAction(): Promise<void> {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter((item => {
        return item.actionStatus === 'PotentialActionStatus' && item.inscriptionYearId < item.currentYear;
      }));
      this._loadingService.hideLoading();
      // open modal
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Register.DeleteRegisterAction.TitleLong',
          description: 'Register.DeleteRegisterAction.Description',
          errorMessage: 'Register.DeleteRegisterAction.CompletedWithErrors',
          refresh: this.refreshAction,
          execute: this.executeDeleteRegisterAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  executeDeleteRegisterAction(): void {
    //
  }
}
