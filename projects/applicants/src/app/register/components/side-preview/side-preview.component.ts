import {Component, Input, OnInit} from '@angular/core';
import {ConfigurationService} from '@universis/common';

@Component({
  selector: 'app-register-side-preview',
  templateUrl: './side-preview.component.html'
})
export class SidePreviewComponent implements OnInit {
  @Input() model: any;
  public currentLang: string;

  constructor(private _configurationService: ConfigurationService) { }

  ngOnInit(): void {
    this.currentLang = this._configurationService.currentLocale;
  }

}
