export default {
  "Register": {
    "StudyPrograms.LongDescription": "You can see the list of study programs, where you are reviewer.",
    "ActionStatusTypes" : {
      "PotentialActionStatus": "Potential",
      "FailedActionStatus": "Failed",
      "CompletedActionStatus": "Completed",
      "ActiveActionStatus": "Active",
      "CancelledActionStatus": "Cancelled",
      "null": "-"
    },
    "StudyProgram": "Study Program",
    "StudyPrograms": "Study programs",
    "ApplicationListTitle": "Requests",
    "EmptyListTitle": "There are not equests",
    "EmptyStudyProgramTitle": "There are not study programs, where you are reviewer",
    "ValidFrom": "From: ",
    "ValidThrough": "Through: ",
    "Candidate": "Candidate",
    "Agent": "Agent",
    "Institute": "Institute",
    "Grade": "Grade",
    "AvgGrade": "Average grade",
    "Application": "Application",
    "DateCreated": "Created at",
    "DateModified": "Modified at",
    "DateSubmitted": "Submitted at",
    "Details": "Details",
    "Status": "Status",
    "Courses": "Courses",
    "Attachments": "Attachments",
    "InscriptionYear": "Inscription year",
    "InscriptionPeriod": "Inscription period",
    "CandidateNumber": "Candidate number",
    "Messages": "Messages",
    "Message": "Message",
    "Accept": "Accept",
    "Reject": "Reject",
    "Review": "Review",
    "OpenStudyProgramApplications": "Open study program's applications",
    "Evaluation": "Evaluation",
    "Evaluations": "Evaluations",
    "Download": "Download",
    "DownloadAll": "Download all",
    "Preview": "Preview",
    "Edit": "Edit",
    "Revert": "Revert to active",
    "Reset": "Reset application to pending",
    "Rating": "Rating",
    "EvaluationRating": "Evaluation rating",
    "UserReviewAdded": "{{name}} added a review for this item",
    "UserEvaluationAdded": "{{alternateName}} added an evaluation for this item",
    "NoReview": "This item has not been reviewed yet.",
    "NoEvaluation": "This item has not been evaluated yet.",
    "NoEvaluationByYou": "Add your evalution and see other's evaluations.",
    "Add": "Add",
    "AddReview": "Add a review",
    "AddEvaluation": "Add evaluation",
    "AcceptConfirm": {
      "Title": "Accept and complete",
      "Message": "You are going to accept this application. After this operation the candidate will be accepted as student of the selected study program. Do you want to proceed?"
    },
    "RejectConfirm": {
      "Title": "Reject application",
      "Message": "You are going to reject this application. The candidate will be informed about application rejection. Do you want to proceed?"
    },
    "ResetConfirm": {
      "Title": "Change application status",
      "Message": "You are going to set this application in pending state. After this operation the candidate will be able to make any changes he/she wants and submit it again. Do you want to proceed?"
    },
    "RevertConfirm": {
      "Title": "Activate application",
      "Message": "You are going to activate this application again. Do you want to proceed?"
    },
    "ReloadError": {
      "Title": "Refresh failed",
      "Message": "The operation has been completed successfully but something went wrong during refresh."
    },
    "RejectAction": {
      "Title": "Reject requests",
      "Description":"The operation will try to reject the selected requests. The process is valid only for pending requests or requests with potential status that refer to expired enrollment period."

    },
    "AcceptCourseRegistration": {
      "Title": "Accept course registration",
      "Message": "You are going to accept the selected course registration. Do you want to proceed?"
    },
    "RejectCourseRegistration": {
      "Title": "Reject course registration",
      "Message": "You are going to reject the selected course registration. Do you want to proceed?"
    },
    "AcceptInternshipRegistration": {
      "Title": "Accept internship registration",
      "Message": "You are going to accept the selected internship registration. Do you want to proceed?"
    },
    "RejectInternshipRegistration": {
      "Title": "Reject internship registration",
      "Message": "You are going to reject the selected internship registration. Do you want to proceed?"
    },
    "SearchCandidate": "Search candidate by name or email...",
    "NoMessages": "No messages yet",
    "SendMessage": "Send a message",
    "NoAttachments": "No attachments yet",
    "NewMessage": "Compose new message",
    "UploadFileHelp": "Drop file to attach, or browse",
    "UploadFilesHelp": "Drop files to attach, or browse",
    "NoSubject": "<No subject>",
    "LatestMessages": "Latest messages via applications",
    "Sender": "Sender",
    "Subject": "Subject",
    "ShowMore": "Show more",
    "ComposeNewMessage": {
      "Title": "Compose new message",
      "Description": "The operation will try to send a message to the candidates of the selected applications. Please write a short message below and start sending messages.",
      "Subject": "Subject",
      "WriteMessage": "Write a short message",
      "Send": "Send",
      "Cancel": "Cancel"
    },
    "SendDirectMessageToCandidate": {
      "Title": "Send a direct message to candidate",
      "Description": "The operation will try to send a direct message (via email) to the selected candidates. Please write a short message below and start sending messages."
    },
    "AcceptedCourses": "Accepted",
    "RejectedCourses": "Rejected",
    "Internships": "Internships",
    "ResidencePermit": "Candidate has a visa/ residence permit to live in the country of his/her home university",
    "LastMessage": "Last message at",
    "Remove": "Remove",
    "AddAttachment": "Upload attachment",
    "AddAttachmentInfo": "Upload the attachment",
    "ΜissingAttachment": "Upload the attachment before submitting",
    "Owner": "Uploaded by",
    "OtherActiveApplications": "Other active applications",
    "StudyProgramSpecialty": "Specialization",
    "DeleteRegisterAction": {
      "Title": "Delete",
      "TitleLong": "Delete applications",
      "Description": "This action will delete the selected enrollment applications of previous years, given that they are in a \"Potential\" state and are not referenced by other entities.",
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors.",
        "Description": {
          "One": "A deletion of an enrollment application failed due to an error, or because it has been referenced by another item.",
          "Many": "{{errors}} were not deleted due to an error, or because they have been referenced by another entity."
        }
      }
    },
    "InscriptionYearId": "Inscription's year ID",
    "CurrentYearId": "Department's current year ID",
    "CandidateId": "Candidate's ID",
    "InscriptionMode": "Inscription's mode",
    "Semester": "Semester",
    "AcademicPeriod": "Academic Period",
    "ApplicationPeriod": "Application Period",
    "To": "to",
    "StudyLevel": "Study level",
    "ApplicationsTotal": "Applications total",
    "ApplicationsActionStatuses": {
      "Active": "Submitted",
      "Cancelled": "Rejected",
      "Completed": "Accepted"
    },
    "From": "from"
  },
  "Settings": {
    "Lists": {
      "StudyProgramRegisterActions": {
        "Title": "Study program application",
        "Description": "Candidate applications",
        "LongDescription": "Manage the list of candidates by accepting or rejecting their applications."
      }
    },
    "EditItem": "Edit item",
    "Close": "Close"
  },
  "Requests": {
    "RequestTitle": "TITLE",
    "RequestTitleLower": "Title",
    "Student": "Student",
    "Name": "Name",
    "Choose": "Choose",
    "ChooseDate": "Choose date",
    "Title": "Requests",
    "TitleSingular": "Request",
    "All": "All (this department)",
    "AllRequestDocumentActions": "Document Requests (all departments)",
    "AllDepartments": "All (all departments)",
    "DepartmentName": "Department",
    "Id": "ID",
    "New": "New",
    "Export": "Export",
    "AdditionalType": "Additional Type",
    "StudentIdentifier": "Student Identifier",
    "FamilyName": "Family Name",
    "GivenName": "Given Name",
    "FullName": "Full Name",
    "StudentStatus": "Student Status",
    "StartTime": "Request date",
    "ActionStatusTitle": "Request Status",
    "NoRequests": "No Requests found.",
    "InLanguage": "In Language",
    "CreationDate": "Date created",
    "AgentName": "Processed by",
    "DateModified" : "Date modified",
    "Download": "Download",
    "Preview": {
      "Label": "Preview",
      "AdditionalType": "Additional Type",
      "Description": "Description",
      "Status": "Status",
      "DateCreated": "Date Created",
      "DateModified": "Date Modified",
      "Details": "Details",
      "Code": "Code"
    },
    "Edit": {
      "Label": "Edit",
      "Title": "Document Request",
      "MessageTitle": "Message Request",
      "RequestTitle": "Special Request",
      "Response": "Response",
      "ResponseCardTitle": "Send a message",
      "ResponsePlaceHolder": "Message for user",
      "AttachFiles": "Attach Files",
      "AttachFilesPlaceHolder": "Upload file",
      "Submit": "Send message and complete",
      "SendAndCancel": "Send message and reject",
      "ApplicationDetails": "Application Details",
      "Code": "CODE",
      "Type": "TYPE",
      "Status": "STATUS",
      "StudentDetails": "Student Details",
      "Firstname": "FIRSTNAME",
      "Lastname": "LASTNAME",
      "DepartmentId": "DEPARTMENT ID",
      "UniversityId": "UNIVERSITY ID",
      "InscriptionYear": "INSCRIPTION YEAR",
      "ResponseTitle": "Response",
      "NewResponse": "New Response",
      "AcceptRequest": "Accept request",
      "CancelRequest": "Cancel request",
      "ReleaseRequest": "Release request",
      "DownloadFile": "Download File",
      "ModalRejectTitle": "Do you really want to reject the request?",
      "ModalAcceptTitle": "Do you really want to accept the request?",
      "ModalReleaseTitle": "This operation is going to release request which will be available again to be claimed by another user. Do you want to proceed?",
      "ModalActionCanNotBeUndone": "This action can not be undone.",
      "choice": "Choice",
      "SendMessage": "Send message",
      "OperationCompleted": "The operation was completed successfully",
      "release": "Release",
      "accept": "Accept",
      "reject": "Reject",
      "claim": "Claim action",
      "NotClaimed": "This action has not be claimed by someone user. Press [Claim action] to complete this request.",
      "ErrorCanNotChangeStatus": "The request is completed. It's status can not be changed.",
      "ErrorCouldNotSendMessage": "There was an error while the message was being sent.",
      "Start": "Start",
      "ActionMessage": {
        "None": "There is no selected item that meets action requirements.",
        "One": "You have selected one item that meets action requirements. Press Start button to proceed.",
        "Many": "You have selected {{length}} items that meet action requirements. Press Start button to proceed."
      },
      "CompletedWithErrors": {
        "Title": "The operation was completed with errors.",
        "Description": {
          "One": "One item has not been updated due to an error occurred while executing process.",
          "Many": "{{errors}} items have not been updated due to an error occurred while executing process."
        }
      },
      "AcceptAction": {
        "Title": "Accept requests",
        "Description":"The operation will try to complete the selected requests. All the pre-defined procedures for each type of request will be performed eg document numbering or archiving. The process is valid for pending requests only."
      },
      "RejectAction": {
        "Title": "Reject requests",
        "Description":"The operation will try to reject the selected requests. Request owners will be informed for this operation by the system interfaces. The process is valid for pending requests only."
      },
      "PublishAction": {
        "Title": "Publish requests",
        "Description":"The operation will try to publish documents related with the selected requests. Request owners will be able to get related documents by the system interfaces. The process is valid for completed requests that require a digital signature and it has been applied or for those requests where a digital signature is not required."
      },
      "SignAction": {
        "Title": "Sign request documents",
        "Description":"The operation will try to digitally sign documents that are related with the selected requests. The process is valid for completed requests that require a digital signature or not. End users will be able to get document after publishing."
      },
      "ReissueAction": {
        "Title": "Reissue documents",
        "Description":"The operation will reissue non published documents of the selected requests that have been accepted. After this operation digital signing and publishing will be available to complete where is required."
      },
      "ClaimAction": {
        "Title": "Claim requests",
        "Description": "The operation will try to assign you as the agent for the selected requests. After this operation, you will be able to edit and take actions on those requests."
      },
      "ReleaseAction": {
        "Title": "Release requests",
        "Description": "This operation will try to unassign you from the agent for the selected requests. After this operation, you won't be able to edit nor take actions on those requests but a coworker of yours will be able to claim them."
      },
      "ActivateAction": {
        "Title": "Reset to active status",
        "Description": "The operation will try to reset application status to active.",
        "RequestDocumentActionsMessage": "Attention! The previously generated file of the application will be cancelled and you cannot use it for publication, digital signature, etc. After the process is over, you will be able to approve or reject the application again. Do you want to proceed?",
        "RequestRemoveActionsMessage": "After this operation, you will be able to approve or reject this request again. Do you want to proceed?",
        "GraduationRequestActionsMessage": "After this operation, you will be able to approve or reject this request again. Do you want to proceed?"
      },
      "activate": "Reset to active",
      "CancelPublishAction": {
        "Title": "Cancel publication",
        "Description": "The operation will try to cancel the publication of the documents related with the selected requests. After completion, the students will no longer have access to those documents. The action is valid for completed requests and published documents."
      },
      "DownloadAction": {
        "Title": "Download document requests",
        "Description": "The document download process will attempt to download the selected documents that have a file. If multiple documents are selected, then they will be placed in a zip file, while if it is only one, the file will be downloaded directly."
      },
      "DownloadAttachmentAction": {
        "Title": "Attachment download",
        "Description": "The attachment download process will attempt to download the files and place them in a zip file with the candidate's name."
      }
    },
    "RequestTypes": {
      "RequestDocuments": "Request Document Types",
      "RequestDocumentsDescription": "Manage request document types per language, add new document types or edit existing items.",
      "ProofOfRegistrationLetter": "Proof of Registration Letter",
      "StudentEnrollmentStatement": "Student Enrollment Statement",
      "StudentTranscript": "Student Transcript",
      "DelayMilitaryServiceLetter": "Delay Military Service Letter",
      "DraftTranscript": "Draft Transcript",
      "Types": "Available request types",
      "TypesDescription": "Manage request types per language, add new request types or edit existing.",
      "Category": "Κατηγορία"
    },
    "Active": "Active",
    "AcademicYear": "Academic year",
    "AcademicPeriod": "Academic period",
    "ExamPeriod": "Exam period",
    "AgreeParticipate": "Agree",
    "MessageMailSubject": "Update regarding your request",
    "DocumentTemplate": "Document template",
    "RequestDocumentActions": "Document requests",
    "ActiveStudentRequestActions": "Active requests",
    "RelatedFile": "Related file",
    "File": "File",
    "PrintDialogMessage": "Out of the {{total}} requests you chose, {{found}} can be printed.",
    "DigitalSignature": "Digital Signature",
    "DigitalSignatureTrue": "Contains Digital Signature",
    "DigitalSignatureFalse": "Does not contain Digital Signature",
    "DocumentNumber": "Protocol number",
    "Published": "Published",
    "DigitalSignatureRequired": "Digital signature is required",
    "DigitalSignatureNotRequired": "Digital signature is not required",
    "Publish": "Publish",
    "CancelPublish": "Cancel publication",
    "CancelPublishMessage": "You are going to cancel the publication of the document. Do you want to proceed?",
    "PublishMessage": "You are going to publish document. Do you want to proceed?",
    "DatePublished": "Date published",
    "ReIssue" : "Reissue report",
    "ReIssueMessage": "You are going to reissue document. Same protocol number will be assigned. Do you want to proceed?",
    "ReIssueSuccessMessage": "Reissue report completed successfully",
    "AcceptRequestFailed": "Approving request failed",
    "RequestedAfter": "Requested after",
    "RequestedBefore": "Requested before",
    "PublishedAfter": "Published after",
    "PublishedBefore": "Published before",
    "PublishDocument": "Publish document",
    "PublishDocumentNoUser": "It appears that the student's account has been deleted. Thus, the document cannot be published.",
    "CreatedBy": "Created by",
    "UnsupportedRequestType": {
      "Title": "Usupported request type",
      "Message": "You cannot edit this type of request by using registrar application. If you want to continue by editing this request please contact system administrators for further information."
    },
    "DocumentCancelled": "The document was cancelled",
    "Status": "Status",
    "History": "History",
    "Date": "Date",
    "Description": "Description",
    "Username": "User name",
    "CurrentSpecialty": "From specialty",
    "TargetSpecialty": "To specialty",
    "CanSelectSpecialty": "Specialty rules met",
    "ValidateSpecialtyRulesAction": {
      "Title": "Validate specialty rules",
      "Description": "The operation will validate the specialty selection rules of all the students of the selected active requests. This is a required step in order to perform mass approvals/rejections."
    },
    "PreferredSpecialtyRequestAction": {
      "Accept": {
        "Description": "The operation will try to complete the selected active requests. Used only for students that satisfy the specialty selection rules (first, use the \"Validate specialty rules\" action). Note: In specific cases, you can approve the requests from their \"Preview\" page."
      },
      "Reject": {
        "Description": "The operation will reject the selected active requests."
      }
    }
  },
  "Documents": {
    "Accept": "Accept file",
    "Reject": "Reject file",
    "AcceptMessage": "The file was successfully accepted",
    "RejectMessage" :" The file was rejected",
    "AcceptMessageFailure": "Accept file failed",
    "RejectMessageFailure" :"Reject file failed",
    "HasSubmitted": "Yes, accept",
    "Success":"Check of the required documents is successful",
    "Warning":"Check of the required documents is not yet completed or is not successful",
    "RelatedRequests": "Related document requests"
  }
}
