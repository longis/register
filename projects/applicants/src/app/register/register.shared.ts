import {NgModule, CUSTOM_ELEMENTS_SCHEMA, ModuleWithProviders, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablesModule } from '@universis/ngx-tables';
import { SharedModule } from '@universis/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { AdvancedFormsModule } from '@universis/forms';
import { RouterModule } from '@angular/router';
import { environment } from '../../environments/environment';
import { MostModule } from '@themost/angular';
import { EditComponent } from './components/edit/edit.component';
import { SidePreviewComponent } from './components/side-preview/side-preview.component';
import { AttachmentDownloadComponent } from './components/attachment-download/attachment-download.component';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { ComposeMessageComponent } from './components/compose-message/compose-message.component';
import { ModalEditComponent } from './components/edit/modal-edit.component';
import { SendMessageActionComponent } from './components/send-message-action/send-message-action.component';
import { RequestActionComponent } from './components/request-action/request-action.component';
import * as translations from './i18n';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MostModule,
    RouterModule,
    TranslateModule,
    TablesModule,
    FormsModule,
    AdvancedFormsModule,
    NgxExtendedPdfViewerModule,
    NgxDropzoneModule
  ],
  declarations: [
    EditComponent,
    ModalEditComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ComposeMessageComponent,
    SendMessageActionComponent,
    RequestActionComponent
  ],
  exports: [
    EditComponent,
    ModalEditComponent,
    SidePreviewComponent,
    AttachmentDownloadComponent,
    ComposeMessageComponent,
    SendMessageActionComponent,
    RequestActionComponent
  ],
  entryComponents: [
    ModalEditComponent,
    SendMessageActionComponent,
    RequestActionComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RegisterSharedModule  {
  constructor(private _translateService: TranslateService) {
    environment.languages.forEach(language => {
      if (Object.prototype.hasOwnProperty.call(translations, language)) {
        this._translateService.setTranslation(language, translations[language], true);
      }
    });
  }
  static forRoot(): ModuleWithProviders<RegisterSharedModule> {
    return {
      ngModule: RegisterSharedModule,
      providers: [
      ],
    };
  }

}
